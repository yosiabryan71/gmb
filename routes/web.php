<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\StudentController as AdminStudentController;
use App\Http\Controllers\Admin\ReportController as AdminReportController;
use App\Http\Controllers\Admin\ResponseController as AdminResponseController;
use App\Http\Controllers\Admin\SummaryController as AdminSummaryController;

use App\Http\Controllers\Staff\ReportController as StaffReportController;
use App\Http\Controllers\Staff\ResponseController as StaffResponseController;

use App\Http\Controllers\Student\ReportController as StudentReportController;
use App\Http\Controllers\Student\ResponseController as StudentResponseController;

use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::middleware(['level:admin'])->group(function () {
        Route::get('/admin', function () {
            return view('admin.home');
        });
        Route::resource('/admin/users', AdminUserController::class);
        Route::resource('/admin/students', AdminStudentController::class);
        Route::resource('/admin/reports', AdminReportController::class);
        Route::resource('/admin/responses', AdminResponseController::class);
        Route::get('/admin/generate', [AdminSummaryController::class, 'index']);
    });

    Route::middleware(['level:staff'])->group(function () {
        Route::get('/staff', function () {
            return view('staff.home');
        });
        Route::resource('/staff/reports', StaffReportController::class);
        Route::resource('/staff/responses', StaffResponseController::class);
    });

    Route::middleware(['level:student'])->group(function () {
        Route::get('/student', function () {
            return view('student.home');
        });
        Route::resource('/student/reports', StudentReportController::class);
        Route::resource('/student/responses', StudentResponseController::class);
    });
});

Route::get('/login', [LoginController::class, 'show'])->name('login');

Route::post('/login', [LoginController::class, 'check']);

Route::get('/logout', [LoginController::class, 'logout']);

