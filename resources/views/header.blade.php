<?php
$level = auth()->user()->level;
?>

<header class="mb-4 border-bottom" id="navigation-bar">
    <div class="container d-flex align-items-center">
        <a href="#" class="d-flex align-items-center me-3 text-decoration-none">
            <img src="/img/clouds.svg" style="width: 40px">
            <span class="fs-4 ms-2">GMB</span>
        </a>
        <ul class="nav me-auto">
            @if (auth()->user()->level == 'admin')
                <li><a href="/{{ $level }}/users" class="nav-link">User</a></li>
                <li><a href="/{{ $level }}/students" class="nav-link">Student</a></li>
            @endif
            <li><a href="/{{ $level }}/reports" class="nav-link">Report</a></li>
            <li><a href="/{{ $level }}/responses" class="nav-link">Response</a></li>
            @if (auth()->user()->level == 'admin')
                <li><a href="/{{ $level }}/generate" class="nav-link">Generate</a></li>
            @endif
        </ul>
        <div class="dropdown">
            <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                <i class="bi bi-person-circle" style="font-size: 20px"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#" class="dropdown-item">Halo, {{ auth()->user()->username }}</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/logout" class="dropdown-item">Log out></a></li>
            </ul>
        </div>
    </div>
</header>
