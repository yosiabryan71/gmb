@extends('app')

@section('content')
    <div class="container">
        <h1>Data Response</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Report_id</th>
                    <th>Respponse_date</th>
                    <th>Response</th>
                    <th>user_id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->id }}</td>
                        <td>{{ $response->report_id}}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->user_id}}</td>
                        <td>
                            <a href="/staff/responses/{{ $response->id }}" class="btn btn-primary">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                            data-bs-target="#modal-{{ $response->id }}">Delete</a>
                        </td>
                        <div class="modal fade" id="modal-{{ $response->id }}" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Confirm</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Response ID {{ $response->id }} will be removed.</p>
                                        <p>Continue?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="/staff/responses/{{ $response->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Cancel</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/staff/responses/create" class="btn btn-success">Add</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
