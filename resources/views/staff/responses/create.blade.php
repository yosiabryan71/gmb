@extends('app')

@section('content')
    <div class="container">
        <h1>Add Response</h1>
        <form action="/staff/responses" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                
                <div class="col-3 mb-3">
                    <label for="report_id" class="form-label">report_id</label>
                    <select name="report_id" class="form-control">
                        @foreach ($report_list as $report)
                            <option value="{{ $report->id }}">{{ $report->id }} - {{ $report->report }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3 mb-3">
                    <label for="response_date" class="form-label">Response_date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date">
                </div>
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response">
                </div>
                
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
