@extends('app')

@section('content')
    <div class="container">
        <h1>Add Report</h1>
        <form action="/staff/reports" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="report_date" class="form-label">Report_date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date">
                </div>
                <div class="col-3 mb-3">
                    <label for="user_id" class="form-label">user_id</label>
                    <select name="user_id" class="form-control">
                        @foreach ($user_list as $user)
                            <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->username }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-3 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report">
                </div>
                <div class="col-3 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo">
                </div>
                <div class="col-3 mb-3">
                    <label for="status" class="form-label">Status</label>
                    <select name="status" class="form-control">
                        @foreach (['sent', 'processed', 'completed'] as $item)
                            <option value="{{ $item }}">{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
