@extends('app')

@section('content')
    <div class="container">
        <h1>Report Detail</h1>
        <form action="/staff/reports/{{ $report->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="report_date" class="form-label">Report_date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date" value="{{ $report->report_date }}">
                </div>
                <div class="col-3 mb-3">
                    <label for="user_id" class="form-label">user_id</label>
                    <input type="text" class="form-control" id="user_id" name="user_id" value="{{ $report->user_id }}" disabled>
                </div>
                <div class="col-3 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report" value="{{ $report->report }}">
                </div>
                <div class="col-3 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo">
                    <img src="{{ asset('storage/' . $report->photo) }}" style="width: 300px">
                </div>
                <div class="col-3 mb-3">
                    <label for="status" class="form-label">Status</label>
                    <select name="status" class="form-control">
                        @foreach (['sent', 'processed', 'completed'] as $item)
                            <option value="{{ $item }}" {{ $report->status == $item ? 'checked' : ''}}>{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
