@extends('app')

@section('content')
    <div class="container">
        <h1>Add Response</h1>
        <form action="/admin/responses/{{ $response->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                
                <div class="col-3 mb-3">
                    <label for="report_id" class="form-label">report_id</label>
                    <input type="text" class="form-control" id="report_id" name="report_id" value="{{ $response->report_id}}" disabled>
                </div>
                <div class="col-3 mb-3">
                    <label for="response_date" class="form-label">Response_date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date" value="{{ $response->response_date}}">
                </div>
                <div class="col-3 mb-3">
                    <label for="response" class="form-label">Response</label>
                    <input type="text" class="form-control" id="response" name="response" value="{{ $response->response}}">
                </div>
                
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
