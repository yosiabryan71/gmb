@extends('app')

@section('content')
    <div class="container">
        <h1>User Detail</h1>
        <form action="/admin/users/{{ $user->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}" disabled>
                </div>
                <div class="col-3 mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="col-3 mb-3">
                    <label class="form-label">Level</label>
                    @foreach (['admin', 'staff', 'student'] as $item)
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="level" value="{{ $item }}"
                                {{ $user->level == $item ? 'checked' : '' }}>
                            <label class="form-check-label">{{ $item }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
