@extends('app')

@section('content')
    <div class="container">
        <h1>Student Data</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Report_date</th>
                    <th>Report</th>
                    <th>Photo</th>
                    <th>Status</th>
                    <th>user_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($report_list as $report)
                    <tr>
                        <td>{{ $report->id }}</td>
                        <td>{{ $report->report_date }}</td>
                        <td>{{ $report->report }}</td>
                        <td>{{ $report->photo }}</td>
                        <td>{{ $report->status}}</td>
                        <td>{{ $report->user_id}}</td>
                        
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button type="button" class="btn btn-secondary" onclick="window.print()">
            Print
        </button>

        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
