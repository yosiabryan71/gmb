@extends('app')

@section('content')
    <div class="container">
        <h1>Student Detail</h1>
        <form action="/admin/students/{{ $student->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $student->name }}">
                </div>
                <div class="col-3 mb-3">
                    <label for="grade" class="form-label">grade</label>
                    <input type="text" class="form-control" id="grade" name="grade" value="{{ $student->grade }}">
                </div>
                <div class="col-3 mb-3">
                    <label for="phone" class="form-label">phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $student->phone }}">
                </div>
                <div class="col-3 mb-3">
                    <label for="address" class="form-label">adress</label>
                    <input type="text" class="form-control" id="address" name="address" value="{{ $student->address }}">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
