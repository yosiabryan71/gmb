@extends('app')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title>Form Report</title>
	<style>
		form {
			margin: 50px auto;
			max-width: 500px;
			padding: 20px;
			border: 1px solid #ccc;
			border-radius: 10px;
			box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
		}

		label {
			display: block;
			margin-bottom: 5px;
			font-weight: bold;
		}

		input[type="text"], textarea {
			width: 100%;
			padding: 10px;
			border: 1px solid #ccc;
			border-radius: 5px;
			margin-bottom: 20px;
			box-sizing: border-box;
			resize: none;
		}

		input[type="submit"] {
			background-color: #4CAF50;
			color: white;
			padding: 10px 20px;
			border: none;
			border-radius: 5px;
			cursor: pointer;
			font-size: 16px;
			font-weight: bold;
			transition: background-color 0.3s ease;
		}

		input[type="submit"]:hover {
			background-color: #3E8E41;
		}
	</style>
</head>
<body>
	<form>
		<label for="report_date">Report Date:</label>
		<input type="date" id="report_date" name="report_date">

		<label for="name">Name:</label>
		<input type="text" id="name" name="name">

		<label for="report">Report:</label>
		<textarea id="report" name="report" rows="8"></textarea>

		<input type="submit" value="Submit">
	</form>
</body>
</html>

@endsection