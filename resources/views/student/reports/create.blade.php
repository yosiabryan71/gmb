@extends('app')

@section('content')
    <div class="container">
        <h1>Add Report</h1>
        <form action="/student/reports" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="report_date" class="form-label">Report_date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date">
                </div>
                <div class="col-3 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report">
                </div>
                <div class="col-3 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
