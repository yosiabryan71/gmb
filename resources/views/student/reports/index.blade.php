@extends('app')

@section('content')
    <div class="container">
        <h1>Student Data</h1>
        <p>{{ $report_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Report_date</th>
                    <th>Report</th>
                    <th>Photo</th>
                    <th>Status</th>
                    <th>user_id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($report_list as $report)
                    <tr>
                        <td>{{ $report->id }}</td>
                        <td>{{ $report->report_date }}</td>
                        <td>{{ $report->report }}</td>
                        <td>{{ $report->photo }}</td>
                        <td>{{ $report->status}}</td>
                        <td>{{ $report->user_id}}</td>
                        <td>
                            <a href="/student/reports/{{ $report->id }}" class="btn btn-primary">Detail</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                            data-bs-target="#modal-{{ $report->id }}">Delete</a>
                        </td>
                        <div class="modal fade" id="modal-{{ $report->id }}" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Confirm</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Report ID {{ $report->id }} will be removed.</p>
                                        <p>Continue?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="/student/reports/{{ $report->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Cancel</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/student/reports/create" class="btn btn-success">Add</a>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
