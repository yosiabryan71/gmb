<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Report;
use App\Models\Response;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user_1 = User::create([
            'username' => 'andrew',
            'password' => Hash::make('Coba_123'),
            'level' => 'student'
        ]);

        $user_2 = User::create([
            'username' => 'abdi',
            'password' => Hash::make('Coba_123'),
            'level' => 'staff'
        ]);

        $user_3 = User::create([
            'username' => 'udin',
            'password' => Hash::make('Coba_123'),
            'level' => 'admin'
        ]);

        $student_1 = Student::create([
            'name' => 'andrew',
            'grade' => 'XII',
            'phone' => '0812345678912',
            'address' => 'jl. apa aja',
            'user_id' => $user_1->id
        ]);

        $report_1 = Report::create([
            'report_date' => '2010-02-01',
            'user_id' => $user_1->id,
            'report' => 'Levin memukul Agus saat istirahat',
            'photo' => 'photo'
        ]);

        $response_1 = Response::create([
            'report_id' => $report_1->id,
            'response_date' => '2010-02-01',
            'response' => 'Terima kasih atas laporannya akan segera kami tindak lanjut',
            'user_id' => $user_1->id
        ]);
    }
}