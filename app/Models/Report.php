<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'report_date',
        'user_id',
        'report',
        'photo',
        'created_at',
        'updated_at',
    ];
}
