<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::paginate(5);
        return view('student.reports.index', ['report_list' => $reports]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('student.reports.create', ['user_list' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'report_date' => 'required',
            'report' => 'required',
            'photo' => 'required|file',
        ]);
        $path = $request->photo->store('public/images');
        $data['photo'] = str_replace('public/', '', $path);
        $data['user_id'] = Auth::user()->id;

        Report::create($data);
        return redirect('/student/reports');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::find($id);
        return view('student.reports.detail', ['report' => $report]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'report_date' => 'required',
            'report' => 'required',
            'photo' => 'nullable|file',
            'status' => 'required',
        ]);


        if (array_key_exists('photo', $data)) {
            $path = $request->photo->store('public/images');
            $data['photo'] = str_replace('public/', '', $path);
        }

        Report::where('id', $id)->update($data);

        return redirect('/student/reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Report::destroy($id);
            return redirect('/student/reports');
        } catch (QueryException $exc) {
            return redirect('/student/reports')
                ->withErrors([
                    'msg' => 'Report ' . $id . ' cannot be deleted because related with other entity'
                ]);
        }
    }
}
