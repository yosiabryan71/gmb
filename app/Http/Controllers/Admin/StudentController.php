<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::paginate(5);
        return view('admin.students.index', ['student_list' => $students]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'grade' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required',
        ]);
        $user = User::create([
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'level' => 'student'
        ]);
        Student::create([
            'name' => $data['name'],
            'grade' => $data['grade'],
            'address' => $data['address'],
            'phone' => $data['phone'],
            'user_id' => $user->id
        ]);
        return redirect('/admin/students');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);
        return view('admin.students.detail', ['student' => $student]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => 'required',
            'grade' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);

        Student::where('id', $id)->update($data);

        return redirect('/admin/students');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Student::destroy($id);
            return redirect('/admin/students');
        } catch (QueryException $exc) {
            return redirect('/admin/students')
                ->withErrors([
                    'msg' => 'Student ' . $id . ' cannot be deleted because related with other entity'
                ]);
        }
    }
}
